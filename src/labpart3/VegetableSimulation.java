/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labpart3;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Admin
 */
public class VegetableSimulation {
    	public static void main(String[ ] args)
	{
	ArrayList<Vegetable> myList = new ArrayList<Vegetable>();

	Vegetable Beet = new Beet( "Red",2);
        Vegetable unripeBeet = new Beet( "Red",0.8);
	Vegetable Carrot = new Carrot( "orange",1.5);
        Vegetable unripeCarrot = new Carrot( "orange",1);

	myList.add(Beet);
	myList.add(Carrot);
        myList.add(unripeCarrot);
        myList.add(unripeBeet);



	System.out.println("\n print using toString method \n");
	System.out.println(myList);
        

	}
    
}
