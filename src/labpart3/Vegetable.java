/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labpart3;

/**
 *
 * @author Admin
 */
public abstract class Vegetable {


	private String Name;
	private String color;
	private double size;



	public Vegetable (String Name, String color, double size) {
		this.Name = Name;
		this.color = color;
		this.size = size;
	}

	public String getName() {
		return Name;
	}
	public void setName(String Name) {
		this.Name = Name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size= size;
	}

	public String toString() {
		return (Name + "\t" + "color : "+ color + "\t" + "Vegitable size : " + size + "\n");
	}


}


